# ELK Playground
### A Vagrant box with a "dockerized" ELK stack.
___

### Notes :

* I chose to use [Docker Compose](https://docs.docker.com/compose/) instead of Vagrant Docker provisioning because Docker Compose is a much better real-world example.
* The current version has a config file for Kibana baked into the Docker image. This will change in a future version by using a Docker volume.
* I've downloaded a sample Apache log file from http://www.monitorware.com/en/logsamples/apache.php to use as input for the Logstash sample client.

### Setup :

TODO

### Prerequisites

* [Vagrant](http://www.vagrantup.com/downloads.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

### Getting started

```
#!bash
git clone https://bitbucket.org/MaartenTutak/elk-playground.git
cd elk-playground
vagrant up
```
Visit the Kibana UI at http://localhost:5601/ on your host machine.

### Quick links :

* [Vagrant](https://www.vagrantup.com/)
* [Docker docs](https://docs.docker.com/)
* [Logstash](http://logstash.net/)

### Recommended reading :

* [The Logstash Book](http://www.amazon.com/gp/product/B00B9JQTCO/ref=oh_aui_d_detailpage_o04_?ie=UTF8&psc=1)
* [The Docker Book: Containerization is the new virtualization](http://www.amazon.com/gp/product/B00LRROTI4/ref=oh_aui_d_detailpage_o03_?ie=UTF8&)

___
If you have any questions, suggestions for improvements, ... Please feel free to contact me.
