#!/bin/sh

# install Docker
curl -sSL https://get.docker.com/ubuntu/ | sudo sh

# install Docker-compose
apt-get -yqq install python-pip && pip install -q -U docker-compose

# start Docker containers
cd /vagrant && docker-compose up -d